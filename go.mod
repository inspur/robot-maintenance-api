module bitbucket.org/inspur/robot-maintenance-api

go 1.16

require (
	github.com/go-kratos/kratos/v2 v2.2.1
	google.golang.org/genproto v0.0.0-20220222213610-43724f9ea8cf
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
)
